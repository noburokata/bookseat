## Step 0: Project setup

## Step 1: Setup knex and migration

- Install knex and mysql
- Set knexfile to read setting from .env
- Add db:up and db:down script
- Add migration for Movies table with following schema
  | Column | Type | Default |
  |---|---|---|
  |id|increments, primary||
  |name| string ||
  |maxSeat| decimal |
  |deleted| boolean|false|
  |createdDate| timestamp|
  |lastModifiedDate| timestamp|now|
- Add migration for Seats table with following schema
  | Column | Type |
  |---|---|
  |movieId|integer, unsigned|
  |name| string |
  |bookDate| timestamp|
- Add foreign key from movieId to Movies.id

## Step 2: Db unit test

- Install jest package
- Update test and test:watch script in package.json
- Add .env.test
- Add customized knex module which initial knex according to NODE_ENV
- Before each unit test run db migrate
- After each unit tst run db rollback
- After all unit test destroy knex object
- Add unit tests for Movies CRUD
- Add unit test for Seats CRUD
